# Captcha decoder

captcha decoder with imagemagick and tesseract

## Installation

```bash
sudo apt-get install libjpeg-dev
sudo apt-get install zlib1g-dev
sudo apt-get install libpng-dev
sudo apt install tesseract-ocr
sudo apt install imagemagick
```

## Usage

```
CaptchaDecoder.getInstance().mode1Captcha("Captcha2" ,".jpg")
```