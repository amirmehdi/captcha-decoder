package com.gitlab.amirmehdi;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.IOException;

class CaptchaDecoderTest {

    @Test
    void mode1Captcha() throws IOException, InterruptedException {
        Assertions.assertEquals("4305",CaptchaDecoder.getInstance().mode1Captcha("Captcha" ,".jpg"));
        Assertions.assertEquals("6083",CaptchaDecoder.getInstance().mode1Captcha("Captcha1" ,".jpg"));
        Assertions.assertEquals("8004",CaptchaDecoder.getInstance().mode1Captcha("Captcha2" ,".jpg"));
    }

    @Test
    void mode2Captcha() throws IOException, InterruptedException {
        Assertions.assertEquals("499467",CaptchaDecoder.getInstance().mode2Captcha("test2" ,".jpg"));
        Assertions.assertEquals("153424",CaptchaDecoder.getInstance().mode2Captcha("test3" ,".jpg"));
        //Assertions.assertEquals("305900",CaptchaDecoder.getInstance().mode2Captcha("test" ,".jpg"));
    }
}